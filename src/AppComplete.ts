﻿import { Container } from 'shiva';

import { Style } from './styles/Style';
import { Header } from './views/Header';
import { PersonComplete } from './views/PersonComplete';
import { Model } from './Model';


class AppComplete extends Container {

    constructor() {
        super({
            root: true
        });


        const header = new Header();
        this.addChild(header);
        header.style({
            paddingTop: "5rem"
        });

        let delay = 2;
        Model.people.map((item) => {
            const person = new PersonComplete(item.name, delay, item.image);
            this.addChild(person);

            delay += 0.2;
        });

    }

}

window.onload = () => {
    new AppComplete();
};
