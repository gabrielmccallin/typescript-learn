import { Container, Image, Ease } from 'shiva';
import { Style } from './../styles/Style';

export class PersonComplete extends Container {

    constructor(name: string, delay: number, imagePath: string) {
        super({
            id: "Person"
        });


        let nameText = new Container({
            text: name,
            fontFamily: Style.fonts.globalFont,
            color: Style.colours.pink,
            marginLeft: "5rem"
        });
        nameText.fromTo({
            delay: delay,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            },
            duration: 2,
            immediateRender: true,
            ease: Ease.EaseOut

        });
        this.addChild(nameText);

        let image = new Image({
            path: imagePath,
            style: {
                width: "100px",
                marginLeft: "5rem",
                marginBottom: "1rem"
            }
        });
        this.addChild(image);
        image.fromTo({
            duration: 2,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            },
            delay: delay,
            immediateRender: true
        });
    }
}
