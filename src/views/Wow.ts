import { Container, Image, Ease } from 'shiva';
import { Style } from './../styles/Style';

export class Wow extends Container {

    constructor(name:string, delay: number) {
        super();

        
        let nameText = new Container({
            text: name,
            fontFamily: "sans-serif",
            color: Style.colours.grey
        });
        nameText.fromTo({
           delay: delay,
           fromVars: {
               opacity: "0"
           },
           toVars: {
               opacity: "1"
           },
           duration: 2,
           immediateRender: true,
           ease: Ease.EaseOut
            
        });
        this.addChild(nameText);

        let image = new Image({
            path: "images/DSC_0724-2.jpg",
            style: {
                width: "100px"
            }
        });
        this.addChild(image);
        image.fromTo({
            duration: 4,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            },
            delay: delay,
            immediateRender: true
        });
    }
}