import { Container, Ease } from 'shiva';
import { Style } from './../styles/Style';

export class Header extends Container {

    constructor() {
        super({
            id: "header",
        });


        let copy = new Container({
            text: "typescript-learn",
            style: Style.h1
        });
        this.addChild(copy);

        copy.fromTo({
            duration: 5,
            ease: Ease.EaseOut,
            fromVars: {
                opacity: "0",
                transform: "translateX(0rem)"
            },
            toVars: {
                transform: "translateX(5rem)",
                opacity: "1"
            }
        });


        let line = new Container({
            height: "1px",
            backgroundColor: Style.colours.pink,
            width: "100%",
            marginTop: "0.2rem",
            opacity: "0.2"
        });
        this.addChild(line);

        line.fromTo({
            ease: Ease.EaseOut,
            duration: 5,
            fromVars: {
                transform: "translateX(100%)"
            },
            toVars: {
                transform: "translateX(5rem)"
            }
        });
    }
}