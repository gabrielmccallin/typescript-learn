﻿export class Style {
    static colours = {
        pink: "pink",
        verydarkgrey: "#333333",
        grey: "#999999",
        iceblue: "#6374ff"
    };

    static fonts = {
        globalFont: "Bree Serif, sans-serif"
    };

    static h1 = {
        fontSize: "2rem",
        fontFamily: Style.fonts.globalFont,
        color: Style.colours.pink,
        letterSpacing: "0rem"
    }

}