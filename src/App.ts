﻿import { Container } from 'shiva';

import { Style } from './styles/Style';
import { Header } from './views/Header';
import { Person } from './views/Person';
import { Model } from './Model';


class App extends Container {

    constructor() {
        super({
            root: true
        });


        const header = new Header();
        this.addChild(header);
        header.style({
            paddingTop: "5rem",
            paddingBottom: "5rem"
        });

        // LEARN !!!!


        // Person.wow()


        // addChild


        // Model.people.map

    }

}

window.onload = () => {
    new App();
};