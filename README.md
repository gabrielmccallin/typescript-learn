# typescript-learn #
---

### Demonstrating some basic Typescript principles ###
A very simple application written in Typescript with a gulpfile to build it.

`npm start` or `gulp` will launch a server at http://localhost:1337 pointing at the application. This will compile to bundle.js and create a sourcemap at bundle.map.js.

Use `gulp publish` to create a minified version of the application.  

See [https://bitbucket.org/gabrielmccallin/shiva](https://bitbucket.org/gabrielmccallin/shiva) for documentation on using shiva :trident:.